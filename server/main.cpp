#include "Common.h"
#include "server.h"
#include "database.h"
#include <pqxx/pqxx>
#define basic_table "CREATE TABLE ACCOUNT ( "\
"  ID   INT PRIMARY KEY  NOT NULL," \
"  BALANCE        INT             NOT NULL );"\
"CREATE TABLE SYMBOL(" \
"  SYM   TEXT   NOT NULL, " \
"  ID    INT    REFERENCES ACCOUNT(ID)," \
"  POS   INT             NOT NULL, " \
"  primary key(SYM, ID));" \
"CREATE TABLE TORDER (" \
"  ID    INT PRIMARY KEY NOT NULL," \
"  ACCOUNT INT REFERENCES ACCOUNT(ID),"\
"  SYM   TEXT   NOT NULL,"\
"  PRICE INT   NOT NULL," \
"  AMOUNT  INT    NOT NULL);"\
"CREATE TABLE EXCUTED(" \
"  ID INT NOT NULL," \
"  ACCOUNT INT REFERENCES ACCOUNT(ID)," \
"  SYM TEXT NOT NULL,"\
"  PRICE INT NOT NULL,"\
"  AMOUNT INT NOT NULL,"\
"  Epoch BIGINT NOT NULL);"\
"CREATE TABLE CANCELED("\
"  ID INT NOT NULL,"\
"  ACCOUNT INT REFERENCES ACCOUNT(ID),"\
"  SYM TEXT NOT NULL,"\
"  PRICE INT NOT NULL,"\
"  AMOUNT INT NOT NULL," \
"  Epoch BIGINT NOT NULL );"

int order_id = 0;
void set_up () {
  unique_ptr<connection> c (new connection("dbname=enginedb host=db port=5432 user=postgres pas\
sword = passw0rd"));
  try{
    work W(*c);
    W.exec(basic_table);
    W.commit();
  }  catch (std::exception& _e) {
    cout << _e.what() << endl;
  }

    
}
int get_id () {
  unique_ptr<connection> c (new connection("dbname=enginedb host=db port=5432 user=postgres password = passw0rd"));
  string sql = "SELECT MAX(id) FROM TORDER;";
  nontransaction N(*c);
  result R(N.exec(sql));
  if (!R[0][0].is_null()){
    return R[0][0].as<int>() + 1;
  }
  else {
    return 0;
  }
}

int main(void) {
  set_up();
  order_id = get_id();
  cout << "order start from " << order_id << endl; 
  try {  
    cout << "server start." << endl;  
    // 建造服务对象  
    boost::asio::io_service ios;  
    // 构建Server实例
    
    Server server(ios);  
  }  catch (std::exception& _e) {  
    cout << _e.what() << endl;
  }  
  cout << "server end." << endl;  
  return 0;  
}  
