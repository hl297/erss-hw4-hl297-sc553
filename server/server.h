#ifndef _SERVER_H__
#define _SERVER_H__


#include "Common.h"
#include "parser.h"

void request_handler(socket_ptr socket);

class Server {

private:


    boost::asio::io_service& ios_;


    boost::asio::ip::tcp::acceptor acceptor_;



public:

    Server(boost::asio::io_service& _ios);
    

    void start(void);

    
};
#endif
