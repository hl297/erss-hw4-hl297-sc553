#include "server.h"


void request_handler(socket_ptr socket) {
   char buf[1024];
   size_t len = 0;
   boost::system::error_code error;
    //add
   try {
     while(true) {
       
       len += socket->read_some(boost::asio::buffer(buf), error);
       if (error == boost::asio::error::eof) {
          break;
       } else if (error) {
	 throw boost::system::system_error(error);
       }
       
     }
   } catch (boost::system::system_error e) {
     socket->shutdown(boost::asio::ip::tcp::socket::shutdown_both);
     socket->close();
   }
  
   
   Parser request(buf, len);
   request.start();
   string response = request.get_response();
   //string res = request.get_response();
   socket->write_some(boost::asio::buffer(response), error);
   socket->shutdown(boost::asio::ip::tcp::socket::shutdown_both);
   socket->close();
}


Server::Server(boost::asio::io_service& _ios) : ios_(_ios),
						acceptor_(_ios, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 12345)) {
    
        start();
}

void Server::start(void) {
  
  while (true) {
    socket_ptr socket(new boost::asio::ip::tcp::socket(ios_));
    //auto buf = std::unique_ptr<char []> (new char[size]);
    acceptor_.accept(*socket);
    //std::thread t(request_handler, socket);
    // t.detach();
    request_handler(socket);
  }
}







